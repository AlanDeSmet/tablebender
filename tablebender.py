#! /usr/bin/python


"""
tablebender.py - tools for managing table representations
Copyright 2017 Alan De Smet

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# TODO: look into CALS compatibility. May be a useful baseline for functionality https://en.wikipedia.org/wiki/CALS_Table_Model

import os
import logging
import csv
import re

class Cell(object):
    def __init__(self, value, tags=set()):
        self.value = str(value)
        self.tags = tags

    def len(self): return len(self.value)

    def __eq__(self, other):
        return \
            self.value == other.value and \
            self.tags == other.tags

CENTER_VALUES = set([x.lower() for x in [
    't', 'f',
    'true', 'false',
    '1', '0',
    'y', 'n',
    'yes', 'no',
    '',
    ]]);
def might_center(cell):
    return cell.value.lower() in CENTER_VALUES

def might_decimal_align(cell):
    try:
        v = cell.value.strip()
        if v == "": v = "0"
        if v[-1] == "%": v = v[:-1]
        float(v)
    except Exception as e:
        return False
    return True


def i_get_field_idx(field, idx, default=None):
    """ Return entry idx from the list "field"; default if invalid

    If any exception is raised (most likely IndexError), return default.

    >>> i_get_field_idx([10,20,30], 2)
    30
    >>> i_get_field_idx([10,20,30], 3)
    >>> i_get_field_idx([10,20,30], 'bacon',99)
    99

    """
    try:
        return field[idx]
    except:
        return default

def i_add_to_list_of_sets(l, idx, val):
    """ Given a list of sets, add val to the set at index idx

    >>> a = []
    >>> i_add_to_list_of_sets(a, 2, 'foo')
    >>> a
    [set([]), set([]), set(['foo'])]
    >>> i_add_to_list_of_sets(a, 2, 'bar')
    >>> a
    [set([]), set([]), set(['foo', 'bar'])]
    >>> i_add_to_list_of_sets(a, 0, 'qux')
    >>> a
    [set(['qux']), set([]), set(['foo', 'bar'])]
    """
    needed = (idx+1) - len(l)
    l += [set() for _ in xrange(needed)]
    l[idx].add(val)

def i_decimal_length(v):
    """ Return digits past decimal point + 1

    Returns the number of digits past the decimal point, +1 for the decimal
    point itself. The last "." is considered the decimal point.

    >>> i_decimal_length("a")
    0
    >>> i_decimal_length("Yes. No")
    4
    >>> i_decimal_length("3.14159")
    6
    >>> i_decimal_length("3.")
    1
    >>> i_decimal_length("1.2.3.4")
    2
    """
    return (len(v) - v.rfind(".")) if v.rfind(".")>=0 else 0

def align_text(align, width, max_dw, text, zero_pad = False):
    """ Align text in fixed size field

    align: 
        - 'l' - left align
        - 'c' - center
        - 'r' - right align
        - '.' - decimal align
    width: width of resulting string; space padded
    max_dw: How many characters to reserve for the decimal point and
            fractional digits. Only used when align=='.'
    text: The text to align.
    zero_pad: If align=='.', if necessarys adds a decimal point
            and 0s to ensure alignment.


    >>> align_text('l', 4,  0, "1.1")
    '1.1 '
    >>> align_text('r', 5,  0, "1.1")
    '  1.1'
    >>> align_text('.', 6,  3, "1.1")
    '  1.1 '
    >>> align_text('.', 6,  3, "1")
    '  1   '
    >>> align_text('c', 7,  0, "1.1")
    '  1.1  '
    """
    if align == 'l':
        fmt = '{0:<'+str(width)+"}"
        return fmt.format(text)
    if align == 'r':
        fmt = '{0:>'+str(width)+"}"
        return fmt.format(text)
    if align == '.':
        if zero_pad:
            if text.find(".") == -1:
                text += "."
        dw = i_decimal_length(text)
        needed = max_dw-dw
        padding = "0" if zero_pad else " "
        text += padding*needed
        fmt = '{0:>'+str(width)+"}"
        return fmt.format(text)
    if align == 'c':
        fmt = '{0:^'+str(width)+"}"
        return fmt.format(text)
    else:
        raise Exception("Internal error: Unknown alignment type '{0}'".format(align))

class Table(object):
    def __init__(self):
        self.cells = []
        self.column_tags = []
        self.row_tags = []
        #logging.warning("{0}".format(self))


    def get_cell_value(self, row, col):
        c = self.get_cell(row, col)
        if c is None: return ""
        return c.value


    def get_cell_tags(self, row, col):
        c = self.get_cell(row, col)
        if c is None: return set()
        return c.tags

    def get_all_tags(self, row, col):
        c = self.get_cell(self, row, col)
        if c is None: return set()
        return c.tags | self.get_column_tags(col) | self.get_row_tags(row)


    def get_cell(self, row, col, default = None):
        r = i_get_field_idx(self.cells, row, default)
        if r is None: return None
        return i_get_field_idx(r, col, default)

    def rows(self):
        return len(self.cells)

    def columns(self):
        if self.rows() == 0: return 0
        return max( [len(x) for x in self.cells])

    def set_cell(self, row, col, cell):
        needed_rows = (row+1) - len(self.cells)
        self.cells.extend([[]] * needed_rows)

        needed_cols = (col+1) - len(self.cells[row])
        self.cells[row].extend([None]*needed_cols)
        #logging.warning("set {0}, {1}, needing: {2} x {3}, now: {4} x {5}".format(row, col, needed_rows, needed_cols, self.rows(), self.columns()))

        self.cells[row][col] = cell

    def set(self, row, col, value, tags=set()):
        self.set_cell(row, col, Cell(value, tags))

    def bulk_set_values(self, values):
        for rownum, rowdata in enumerate(values):
            for colnum, cell in enumerate(rowdata):
                self.set(rownum, colnum, cell)

    def get_column_tags(self, col):
        return i_get_field_idx(self.column_tags, col, set())

    #def add_column_tag(self, col, tag):
    #    i_add_to_list_of_sets(self.column_tags, col, tag)


    def get_row_tags(self, row):
        return i_get_field_idx(self.row_tags, row, set())

    #def add_row_tag(self, row, tag):
    #    i_add_to_list_of_sets(self.row_tags, row, tag)

    def study_columns(self):

        widths = [0]*self.columns()
        for row in range(self.rows()):
            for col in range(self.columns()):
                v = self.get_cell_value(row, col)
                if v is not None:
                    widths[col] = max([widths[col], len(v)])

        decimal = [True]*self.columns()
        center = [True]*self.columns()
        decimal_widths = [-1]*self.columns()

        # Skip first row; likely to be header
        # TODO: Really should check for 'header' tag
        #       which also means _setting_ the header tag on load
        #       which means even more clever. :-/
        for row in range(1, self.rows()):
            for col in range(self.columns()):
                v = self.get_cell_value(row, col)
                cell = self.get_cell(row, col, Cell("", []))

                center[col] &= might_center(cell)
                decimal[col] &= might_decimal_align(cell)

                decimal_widths[col] = max([decimal_widths[col], i_decimal_length(v)])

        alignments = ['l']*self.columns()
        for i in range(self.columns()):
            if decimal[i]: alignments[i] = '.'
            if center[i]: alignments[i] = 'c'


        return (widths, alignments, decimal_widths)



file_format_readers = {}
file_format_writers = {}

def __register_file_format(ff, d, t):
    name = ff.brief_name()
    if name in d:
        raise Exception('Internal error: Two different TableFileFormat {5} are registered as {0}: {1} ({2}) and {3} ({4})'.format(name, ff.__class__, ff.description(), d[name].__class__, d[name].description, t))
    d[name] = ff


def register_file_format_reader(ff):
    __register_file_format(ff, file_format_readers, 'readers')

def register_file_format_writer(ff):
    __register_file_format(ff, file_format_writers, 'writers')

def register_file_format_reader_writer(ff):
    register_file_format_reader(ff)
    register_file_format_writer(ff)


class TableFileFormat(object):
    def extensions(self):
        """
        Return list of extensions this format may be able to read and write.

        First result should be the most common or default extension.
        An empty list is valid.

        Entries will typically begin with ".".

        Extension comparisons are done case insensitively

        ex: [".html", ".htm"]
        """
        raise Exception("Internal error: TableFileFormat.extensions was called.")

    def default_extension(self):
        """ Extension to use when writing a file. Includes preceeding ".".  """
        try: return self.extensions[0]
        except: return None

    def description(self):
        """ Returns brief, human readable string describing this format """
        raise Exception("Internal error: TableFileFormat.description was called.")

    def brief_name(self):
        """ Returns very brief identifier for this format """
        raise Exception("Internal error: TableFileFormat.brief_name was called.")

    def my_extension(self, filename):
        filename = filename.lower()
        for e in self.extensions():
            if filename.endswith(e.lower()):
                return True
        return False

    def score_file(self, fileobj = None, filename = None):
        """ Return score from 0.0 to 1.0 for how plausible this file is this format.
        
        1.0 is a certain match (rare!)
        0.0 is certainly not a match
        anything above 0.5 is "probably"; anything below, "probably not"

        Should not be interpreted as probability; it's not that clever.
        """
        score = 0.01

        if filename is not None and self.my_extension(filename):
            score += 0.6
            logging.debug("    {0} extension".format(score))

        if fileobj is not None:
            try:
                table = self.read(fileobj)
            except Exception as e:
                logging.debug("    {0} unreadable: {1}".format(score, e))
                return 0.0

            score += min([table.rows(), 4])/20.0
            logging.debug("    {0} rows".format(table.rows()))
            score += min([table.columns(), 4])/20.0
            logging.debug("    {0} columns".format(table.columns()))
            logging.debug("    {0} score".format(score))


        if score >= 1.0:
            score = 0.99

        return score

    def read(self, fileobj):
        """ Return a Table object loaded from fileobj """
        raise Exception("Internal error: TableFileFormat.read was called.")

    def write(self, fileobj, table):
        """ Write a Table object into fileobj """
        raise Exception("Internal error: TableFileFormat.read was called.")


class TableFileFormatTSV(TableFileFormat):
    def extensions(self): return [".tsv"] 
    def description(self): return "tab separated values"
    def brief_name(self): return "tsv"

    def read(self, fileobj):
        values = []
        for line in fileobj:
            if len(line) and line[-1] == '\n': line = line[:-1]
            if len(line) and line[-1] == '\r': line = line[:-1]
            values.append(line.split("\t"))

        # Remove trailing lines, common in copy-paste
        while len(values) and values[-1] == [""]:
            values.pop()

        t = Table()
        t.bulk_set_values(values)
        return t

    def write(self, fileobj, table):
        for row in range(table.rows()):
            rowvals = [ table.get_cell_value(row, col) for col in range(table.columns()) ]
            rowvals = [ r if r is not None else "" for r in rowvals ]
            fileobj.write("\t".join(rowvals)+"\n")
register_file_format_reader_writer(TableFileFormatTSV())


class TableFileFormatCSV(TableFileFormat):
    def extensions(self): return [".csv"] 
    def description(self): return "comma separated values"
    def brief_name(self): return "csv"

    def read(self, fileobj):
        r = csv.reader(fileobj)
        values = []
        for row in r:
            values.append(row)
        t = Table()
        t.bulk_set_values(values)
        return t

    def write(self, fileobj, table):
        w = csv.writer(fileobj)
        for row in range(table.rows()):
            rowvals = [ table.get_cell_value(row, col) for col in range(table.columns()) ]
            rowvals = [ r if r is not None else "" for r in rowvals ]
            w.writerow(rowvals)
register_file_format_reader_writer(TableFileFormatCSV())


class TableFileFormatPlainText(TableFileFormat):
    def extensions(self): return [".txt"] 
    def description(self): return "plain text"
    def brief_name(self): return "plain-text"

    def write(self, fileobj, table):
        (widths, alignments, decimal_widths) = table.study_columns()
        for row in range(table.rows()):
            for col in range(table.columns()):
                align = alignments[col]
                if row == 0 and align == '.':
                    align='r'
                out = align_text(align, widths[col], decimal_widths[col], table.get_cell_value(row, col))
                fileobj.write(out+" ")

            fileobj.write("\n")
register_file_format_writer(TableFileFormatPlainText())

class TableFileFormatGitHubFlavoredMarkdown(TableFileFormat):
    def extensions(self): return [".md"] 
    def description(self): return "GitHub flavored Markdown"
    def brief_name(self): return "gmarkdown"

    def write(self, fileobj, table):
        (widths, alignments, decimal_widths) = table.study_columns()
        for row in range(table.rows()):
            fileobj.write("| ")
            for col in range(table.columns()):
                align = alignments[col]
                if row == 0 and align == '.':
                    align='r'

                out = align_text(align, widths[col], decimal_widths[col], table.get_cell_value(row, col), zero_pad=True)
                fileobj.write(out+" | ")

            fileobj.write("\n")

            if row == 0: # special case headers
                fileobj.write("| ")
                for col in range(table.columns()):
                    width = widths[col]
                    align = alignments[col]
                    if   align == 'l': out = ":"+("-"*(width-2))+"-"
                    elif align == 'c': out = ":"+("-"*(width-2))+":"
                    elif align == '.': out = "-"+("-"*(width-2))+":"
                    elif align == 'r': out = "-"+("-"*(width-2))+":"
                    else:
                        raise Exception("Unknown alignment {0}".format(align))
                    fileobj.write(out+" | ")
                fileobj.write("\n")
register_file_format_writer(TableFileFormatGitHubFlavoredMarkdown())

class TableFileFormatHTML(TableFileFormat):
    def extensions(self): return [".html"] 
    def description(self): return "HTML"
    def brief_name(self): return "html"

    def write(self, fileobj, table):
        (widths, alignments, decimal_widths) = table.study_columns()
        fileobj.write("<table>\n")
        for row in range(table.rows()):
            fileobj.write("<tr>")
            for col in range(table.columns()):
                align = alignments[col]
                if row == 0 and align == '.':
                    align='r'

                out = align_text(align, widths[col], decimal_widths[col], table.get_cell_value(row, col), zero_pad=True)
                style = ''
                if align in "r.":
                    style=' style="align:right"'
                fileobj.write("<td"+style+">"+out)

            fileobj.write("\n")
        fileobj.write("</table>\n")

register_file_format_writer(TableFileFormatHTML())

try: 
    import docutils.statemachine
    import docutils.parsers.rst.tableparser

    class TableFileFormatRSTGrid(TableFileFormat):
        def extensions(self): return [] 
        def description(self): return "reStructuredTex grid tables"
        def brief_name(self): return "rstgrid"

        def read(self, fileobj):
            #everything = fileobj.read().strip()

            lines = filter(bool, (line.strip() for line in fileobj))
            sllines = docutils.statemachine.StringList(list(lines))
            #print(sllines)
            parser = docutils.parsers.rst.tableparser.GridTableParser()
            table = parser.parse(sllines)

            """
            +------------------------+------------+----------+----------+
            | Header row, column 1   | Header 2   | Header 3 | Header 4 |
            +========================+============+==========+==========+
            | body row 1, column 1   | column 2   | column 3 | column 4 |
            +------------------------+------------+----------+----------+
            | body row 2             | Cells may span columns.          |
            +------------------------+------------+---------------------+
            | body row 3             | Cells may  | - Table cells       |
            +------------------------+ span rows. | - contain           |
            | body row 4             |            | - body elements.    |
            +------------------------+------------+---------------------+

            Passing the above table to the `parse()` method will result in the
            following data structure::

                ([24, 12, 10, 10],                            # Column widths

                  #rowspan-1, colspan-1, line offset, list of lines of text
                 [[(0, 0, 1, ['Header row, column 1']),       # Header rows
                   (0, 0, 1, ['Header 2']),
                   (0, 0, 1, ['Header 3']),
                   (0, 0, 1, ['Header 4'])]],

                 [[(0, 0, 3, ['body row 1, column 1']),       # Body rows
                   (0, 0, 3, ['column 2']),
                   (0, 0, 3, ['column 3']),
                   (0, 0, 3, ['column 4'])],
                  [(0, 0, 5, ['body row 2']),
                   (0, 2, 5, ['Cells may span columns.']),
                   None,
                   None],
                  [(0, 0, 7, ['body row 3']),
                   (1, 0, 7, ['Cells may', 'span rows.', '']),
                   (1, 1, 7, ['- Table cells', '- contain', '- body elements.']),
                   None],
                  [(0, 0, 9, ['body row 4']), None, None, None]])

            The first item is a list containing column widths (colspecs). The second
            item is a list of head rows, and the third is a list of body rows. Each
            row contains a list of cells. Each cell is either None (for a cell unused
            because of another cell's span), or a tuple. A cell tuple contains four
            items: the number of extra rows used by the cell in a vertical span
            (morerows); the number of extra columns used by the cell in a horizontal
            span (morecols); the line offset of the first line of the cell contents;
            and the cell contents, a list of lines of text.
            """

            values = []
            for row in table[1] + table[2]:
                outrow = []
                for cell in row:
                    outrow.append(" ".join(cell[3]))
                values.append(outrow)

            t = Table()
            t.bulk_set_values(values)
            return t
    register_file_format_reader(TableFileFormatRSTGrid())

except ModuleNotFoundError:
    pass

def detect_format(fileobj = None, filename = None):
    """ What sort of table file is this?

    Guesses based on name and contents; both are optional, but if neither
    is provided, the answer is None.

    >>> from StringIO import StringIO
    >>> detect_format(StringIO("a\tb\tc\td\te")).brief_name()
    'tsv'
    >>> detect_format(StringIO("a,b,c,d,e")).brief_name()
    'csv'
    >>> detect_format(None, 'foo.TSV').brief_name()
    'tsv'
    >>> detect_format(None, 'bar.cSv').brief_name()
    'csv'


    """
    best_score = 0.0
    best_format = None
    logging.debug("Identifying format of {0}".format(filename))
    for f in file_format_readers.values():
        score = f.score_file(fileobj, filename)
        logging.debug("  {0} {1}".format(score, f.description()))
        if fileobj is not None:
            fileobj.seek(0)
        if score > best_score:
            best_format = f
            best_score = score
    if best_format is not None:
        logging.debug("  Choosing {0} {1}".format(best_score, best_format.description()))
    else:
        logging.debug("  No good options")
    return best_format

def read_file(fileobj, filename = None, formatname = None):
    """ Return table loaded form fileobj

    Automatically detects the format unless formatname is passed in.

    fileobj:    file-like object to read from. fileobj.seek(0) must work.
    filename:   Optional filename; used as part of detection
    formatname: If specified, instead of detecting format, assumes spcified
                format.

    >>> from StringIO import StringIO
    >>> tab = read_file(StringIO('a,b,c d,"e,f",g'))
    >>> tab.rows()
    1
    >>> tab.columns()
    5
    >>> tab.get_cell_value(0,0)
    'a'
    >>> tab.get_cell_value(0,3)
    'e,f'
    """
    if formatname is not None:
        if formatname not in file_format_readers:
            raise Exception('Internal error: There is no file format reader named "{0}"'.format(formatname))
        format = file_format_readers[formatname]
    else:
        format = detect_format(fileobj, filename)
    if format is None:
        raise ValueError('Could not identify file format from "{0}".'.format(filename))
    return format.read(fileobj)


def write_file(fileobj, table, formatname):
    if formatname not in file_format_writers:
        raise Exception('Internal error: There is no file format writer named "{0}"'.format(formatname))
    file_format_writers[formatname].write(fileobj, table)

################################################################################
#
# TESTING


        

def load_tests(loader, tests, ignore):
    import doctest
    tests.addTests(doctest.DocTestSuite())
    return tests

if __name__ == '__main__':
    import unittest

    class TestCell(unittest.TestCase):
        def test_len(self):
            v = "a testing value"
            c = Cell(v)
            self.assertEqual(c.len(), len(v))
            c = Cell(v,set(["asdf", "fdsa"]))
            self.assertEqual(c.len(), len(v))


        def test_equality(self):
            v = "another testing value"
            c = Cell(v,set(["asdf", "fdsa"]))
            d = Cell(v,set(["fdsa", "asdf"]))
            self.assertEqual(c,d)

    class TestTable(unittest.TestCase):
        def test_scaling(self):
            t = Table()
            self.assertEqual(t.rows(), 0)
            self.assertEqual(t.columns(), 0)
            self.assertEqual(t.get_cell_value(2,2), "")
            self.assertEqual(t.get_cell_tags(2,2), set())
            self.assertIsNone(t.get_cell(2,2))

            t.set(2, 3, "testval", set(["a","b"]))
            self.assertEqual(t.rows(), 2+1)
            self.assertEqual(t.columns(), 3+1)
            self.assertEqual(t.get_cell_value(2,2), "")
            self.assertEqual(t.get_cell_tags(2,2), set())
            self.assertIsNone(t.get_cell(2,2))
            self.assertEqual(t.get_cell_value(3,3), "")
            self.assertEqual(t.get_cell_tags(3,3), set())
            self.assertIsNone(t.get_cell(3,3))
            self.assertIsNone(t.get_cell(10,20))
            self.assertEqual(t.get_cell_value(2,3), "testval")
            self.assertEqual(t.get_cell_tags(2,3), set(["b","a"]))
    unittest.main()
